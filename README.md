     _____                 _ ______ _     _       _      _ _
    |  __ \               | |  ____(_)   | |     | |    (_) |
    | |  | | ___  __ _  __| | |__   _ ___| |__   | |     _| |__
    | |  | |/ _ \/ _` |/ _` |  __| | / __| '_ \  | |    | | '_ \
    | |__| |  __/ (_| | (_| | |    | \__ \ | | |_| |____| | |_) |
    |_____/ \___|\__,_|\__,_|_|    |_|___/_| |_(_)______|_|_.__/

# DeadFish.Lib.php

![DeadFish](https://bitbucket.org/jnewing/deadfish-lib/raw/master/deadfish.png)

## About

**author** [Joseph Newing](http://twitter.com/jnewing/)

**author email** jnewing [at] gmail [dot] com

**copyright** Joseph Newing 2011 - 2012

**link(s)** [https://bitbucket.org/jnewing/deadfish-lib](https://bitbucket.org/jnewing/deadfish-lib) - [http://epicgeeks.net](http://epicgeeks.net)

**version** 1.4

**thanks** [djekl](http://twitter.com/djekl/) for feedback, and running ideas by etc...

- - -

## DeadFish Library
The DeadFish library is used for making a safe (or safer) storable hash of a user password. We attempt to make storing a password hash
a little more secure by creating an adaptive hash using blowfish and it's algorithm’s keying schedule.

DeadFish lib has several options users can set to customize this library.

**Options**

    min_length - the minamum required length of a user password
    strict_password - require the password to follow "strict" rules (see rules below)
                        o min length required
                        o require alpha and numeric values
                        o require both upper and lower case values
    work_factor - the work factor

Within this class there are both static function users can use for fast access as
well as none static ones, see examples below.

- - -

## Static Usage Examples

**static function example**

    :::php
    $hash = DeadFish::computeHash('some password');


**static function check and existing hash**

    :::php
    $bool = DeadFish::verifyHash('some password', 'matching hash');

## Object Usage Example

**example with default options**

    $df = new DeadFish();
    $df->set_password('some_password');
    $hash = $df->hash();
        // code here...
    if ($df->verify('some_password', 'some_hash'))
        // do stuff...

**example with diff options**

    $options = array(
       'min_length'        => 5,       // make the min password length 5
       'strict_password'   => TRUE     // use strict passwords
    );

    $df = new DeadFish($options);
    $hash = $df->hash('some_password');

