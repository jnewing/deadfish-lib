<?php

    // include our lib
    require_once('DeadFish.Lib.php');

    function microtime_float()
    {
        list($usec, $sec) = explode(" ", microtime());
        return ((float)$usec + (float)$sec);
    }

    // did the user submit?
    if (isset($_POST['submit']))
    {
        // clear our input
        $password           = strip_tags($_POST['user_password']);
        $min_length         = strip_tags($_POST['min_length']);
        $work_factor        = strip_tags($_POST['work_factor']);
        $strict_password    = isset($_POST['use_strict']);

        // reset work factor to max 18 for the demo
        $work_factor = ($work_factor > 18) ? 18 : $work_factor;

        // get the users options
        $options = array(
            'strict_password'   => isset($_POST['use_strict']),
            'min_length'        => $min_length,
            'work_factor'       => $work_factor,
        );

        // init our class
        $df = new DeadFish($options);

        //$mtime_start = microtime();
        $time_start = microtime_float();

        try
        {
            $df->set_password($password);
            $hash = $df->hash();
        }
        catch (Exception $e)
        {
            $hash = $e->getMessage();
        }

        //$mtime_stop = microtime();
        //$total = $mtime_stop - $mtime_start;
        $time_end = microtime_float();
        $time = $time_end - $time_start;

        // destroy our class
        unset($df);
    }

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="distribution" content="global" />
    <meta name="robots" content="follow, all" />
    <meta name="language" content="en" />

    <title>DeadFish.Lib Demo (Example) - Joseph Newing</title>

    <!-- CSS -->
    <link rel="stylesheet" href="style.css" type="text/css" media="screen" charset="utf-8" />
</head>
<body>
    <div class="wrapper">

        <!-- deadfish -->
        <div class="logo tcenter"><img src="deadfish.png" alt="DeadFish.Lib" /></div><br /><br />

        <!-- options -->
        <div class="wbox">
            <form action="" method="post">
                <label>Password: </label>
                <input type="text" name="user_password" size="35" value="<?php print isset($password) ? $password : ''; ?>" placeholder="Password" /><br />

                <br />

                <label>Min Length</label>
                <input type="text" name="min_length" size="4" value="<?php print isset($min_length) ? $min_length : '8' ;?>" /> &nbsp;&nbsp;&nbsp;&nbsp;

                <label>Strict Password?</label>
                <input type="checkbox" name="use_strict" <?php print ($strict_password) ? 'checked="checked"' : '' ?> value="1" /> &nbsp;&nbsp;&nbsp;&nbsp;

                <label>Work Factor</label>
                <input type="text" name="work_factor" size="4" value="<?php print isset($work_factor) ? $work_factor : '9'; ?>" /><br />

                <br />

                <button type="submit" name="submit">Hash Password</button>
            </form>
        </div>

        <br />

        <!-- output -->
        <?php if (isset($hash)) : ?>
            <div class="wbox output">
                <pre>Hash: <?php print $hash; ?></pre>
                <pre>Hashing Duration: <?php print $time; ?> seconds</pre>
            </div>
        <?php endif; ?>

    </div>

</body>
</html>